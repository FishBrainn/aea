import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class Reader {
	public static final byte STX = (byte)0x02;
	public static final byte ETX = (byte)0x03;
	private String _lastMessageFromReader = "";
	private String _lastRequestToReader = "";
	private boolean _successfulAction = false;
	private String _actionFeedback = "";
	private boolean _readerInit = false;
	private String _readerIP = "127.0.0.0";
	private int _portID = 0; 
	
	
	Reader(String readerIP, int portID) throws Exception{
		_readerInit = false;
		_readerIP = readerIP;
		_portID = portID;
		if (_portID==0) _portID = 9100;
		if(askReader("T2"))
		{
			System.out.println("Reader init with : IP="+_readerIP+" on port="+_portID);
			_readerInit = true;
		}
		else
		{
			throw new Exception("Reader initialisation failed. Unabled to send a request to the reader.");
		}

	}
	

	
	public String scanCoupon()
	{
		String name = "ERROR";
		synchronized(this) {
			try {
				Socket socket = new Socket(_readerIP, _portID);
				OutputStream outputStream = socket.getOutputStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				//For convenience a ArrayList was used to encode the message to be sent to the printer.
				//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.

				//Receiving output from Printer

				byte readerOutput = 1;
				String fromReader = "";
				while((readerOutput = (byte)socket.getInputStream().read()) != 3)
				{
					if(readerOutput != 2) //Avoid the inclusion of STX in the user output
						fromReader += (char)readerOutput;

				}
				System.out.println(fromReader);
				//name = fromReader.substring(fromReader.indexOf("#")).substring(4,10);
				outputStream.flush();
				outputStream.close();
				in.close();
				socket.close();
			} catch(Exception e) {
				System.out.println("ERROR@"+_readerIP+" : "+e.getMessage());
			}
			return name;
	}
	}
	public void display(String text)
	{
		askReader("MG#B#"+text);
	}
	public void scan()
	{
		while(true)
		{
			display(scanCoupon());
		}
	}
	
	//Converts Byte[] to byte[]. Used to send data to the printer as it can only accept byte[].
	byte[] toPrimitives(Byte[] oBytes)
	{

		byte[] bytes = new byte[oBytes.length];
		for(int i = 0; i < oBytes.length; i++){
			bytes[i] = oBytes[i];
		}
		return bytes;
	}
	
	//The general print function that takes the formated AEA command and send it to the printer.
	private boolean sendToReader(byte[] AEAcommand)
	{
		synchronized(this) {
			try {
				Socket socket = new Socket(_readerIP, _portID);
				OutputStream outputStream = socket.getOutputStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				//For convenience a ArrayList was used to encode the message to be sent to the printer.
				//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.
				outputStream.write(AEAcommand,0,AEAcommand.length);

				//Receiving output from Printer
				byte printerOutput = 1;
				String fromPrinter = "";
				while((printerOutput = (byte)socket.getInputStream().read()) != 3)
				{
					if(printerOutput != 2) //Avoid the inclusion of STX in the user output
						fromPrinter += (char)printerOutput;

				}
				System.out.println(fromPrinter);
				outputStream.flush();
				outputStream.close();
				in.close();
				socket.close();
				return true;
			} catch(Exception e) {
				System.out.println("ERROR@"+_readerIP+" : "+e.getMessage());
			}
		}
		return false;
	}
	
	//Convert the string to bytes and add the STX and ETX bytes.
	private byte[] stringToByteArray(String command)
	{
		byte[] commandReadyForPrinter = null;
		ArrayList<Byte> AEACommand = new ArrayList<Byte>();
		AEACommand.add(STX);
		byte[] sToByte = command.getBytes();
		for(int i = 0; i < sToByte.length;i++)
		{
			AEACommand.add(sToByte[i]);
		}

		AEACommand.add(ETX);
		//For convenience a ArrayList was used to encode the message to be sent to the printer.
		//However we need to convert it to byte[]. Those two lines are the most efficient way to do so.
		Byte[] buffer = (Byte[]) AEACommand.toArray(new Byte[0]);
		commandReadyForPrinter = toPrimitives(buffer);
		return commandReadyForPrinter;
	}
	//This function is used for any AEA command that only ask for information without printing.
	public boolean askReader(String request){
		boolean succesful = false;
		String statutMessage = "Ask status : ";
		//_lastCommandSent = request;
		if(sendToReader(stringToByteArray(request)))
		{
			statutMessage+="Sent.";
			succesful = true;
		}
		else
		{
			statutMessage+="Failed.";
			succesful = false;
		}
		//System.out.println(statutMessage);
		_actionFeedback = statutMessage;
		_successfulAction = succesful;
		return succesful;
	}
}
